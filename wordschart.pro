QT += quick

CONFIG += c++17

git_command = git --git-dir $$PWD/.git --work-tree $$PWD
build_define_command = $$git_command rev-list HEAD | find /c /v \"\"

GIT_VERSION = $$system($$git_command describe --always --tags)
splitted = $$split(GIT_VERSION, -)
MAIN_VERSION = $$first(splitted)
BUILD = $$system($$build_define_command)

VERSION = $${MAIN_VERSION}.$${BUILD}

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

QMAKE_TARGET_COMPANY = "Yuriy Ibetullov"
QMAKE_TARGET_PRODUCT = "Words Chart"
QMAKE_TARGET_DESCRIPTION = "Application to count the frequency of occurrence of words in the file"
QMAKE_TARGET_COPYRIGHT = "Copyright (C) 2021 Yuriy Ibetullov"
RC_ICONS = $$PWD/ui/icon.ico

TEMPLATE = app
TARGET = WordsChart

CONFIG(debug, debug|release){
    DESTDIR = $$PWD/bin/debug
} else {
    DESTDIR = $$PWD/bin/release
}

HEADERS += \
        $$files(sources/*.h, true)

SOURCES += \
        $$files(sources/*.cpp, true)

RESOURCES += ui/qml.qrc

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Additional import path used to resolve QML modules just for Qt Quick Designer
QML_DESIGNER_IMPORT_PATH =

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
else: target.path = $$DESTDIR

!isEmpty(target.path): INSTALLS += target

CONFIG += install_ok
