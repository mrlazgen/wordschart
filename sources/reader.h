#pragma once

#include <QObject>
#include <QHash>
#include <QMap>
#include <QMutex>

class Reader : public QObject
{
    Q_OBJECT

public:
    explicit Reader(QObject *parent = nullptr);

    void read();
    QMap<QString,int> getTop();
    QHash<QString,int> getWords();

    int progress() const;
    QString source() const;

    int getTopCount() const;
    void setTopCount(int topCount);

public slots:
    void setProgress(int progress);
    void setSource(QStringView source);

private:
    void addWord(const QString& word, int count = 1);
    void updateTopList(const QString& word);

signals:
    void progressChanged(int progress);
    void sourceChanged(QString source);
    void finished(bool success, const QString& message = "");
    void topListChanged();

    void newWord(const QString& word);

private:
    int m_progress;
    int m_topCount;
    QString m_source;
    bool m_caseSensetive;

    QHash<QString,int> m_words;
    QMap<QString,int> m_top;

    QMutex m_mutex;
    QMutex m_counterMutex;

};

