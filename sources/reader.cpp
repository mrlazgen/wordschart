#include "reader.h"

#include <QFile>
#include <QDebug>
#include <QTextStream>
#include <QMutexLocker>
#include <QThread>

#include <execution>

Reader::Reader(QObject *parent)
    : QObject(parent)
    , m_progress(0)
    , m_topCount(15)
    , m_caseSensetive(false)
{
}

void Reader::read()
{
    setProgress(0);
    m_words.clear();
    m_top.clear();

    QFile file(m_source);
    if(!file.open(QFile::ReadOnly | QFile::Text))
    {
        qWarning() << "Can't open file:" << file.errorString();
        emit finished(false, file.errorString());
    }

    QTextStream in(&file);
    in.setCodec("UTF-8"); //TODO: codec choose

    const auto size = file.size();
    if(size == 0)
    {
        setProgress(100);
        emit finished(false, tr("Selected file is empty"));
        return;
    }

    if(size < std::numeric_limits<int>::max())
    {
        quint64 processedSize = 0;

        const auto text = in.readAll();
        const auto lines = text.split(QRegExp("[\r\n]"),QString::SkipEmptyParts);

        //NOTE: used find_if in necessary interrupt file processing on operation cancel
        std::find_if(std::execution::par, lines.begin(), lines.end(), [&](const QString &line) {

            if ( QThread::currentThread()->isInterruptionRequested() ) {
                return true;
            }

            const auto words = line.simplified()
                    .trimmed()
                    .split(QRegExp("\\W+"), QString::SkipEmptyParts);

            for(const auto &word : words)
            {
                addWord(word);
            }

            QMutexLocker locker(&m_counterMutex);
            processedSize += line.size();
            setProgress(processedSize * 100 / size);

            return false;
        });
    }
    else
    {
        qDebug() << QString("Large file - %1 bytes. Will be precessed line by line").arg(size);
        while (!in.atEnd())
        {
            if ( QThread::currentThread()->isInterruptionRequested() ) {
                break;;
            }

            const auto line = in.readLine();
            const auto words = line.simplified()
                    .split(QRegExp("\\W+"), QString::SkipEmptyParts);

            for(const auto &word : words)
            {
                addWord(word);
            }

            // Calculate the progress in percent.
            const qint64 position = in.pos();
            const int progress = (position * 100) / size;
            setProgress(progress);
        }
    }

    file.close();

    setProgress(100);
    if(m_top.isEmpty())
    {
        emit finished(false, tr("File parsint error. Words not founded or bad encoding. Try to resave file in UTF-8"));
    }
    else
    {
        emit finished(true);
    }
}

QMap<QString, int> Reader::getTop()
{
    QMutexLocker locker(&m_mutex);
    return m_top;
}

QHash<QString, int> Reader::getWords()
{
    QMutexLocker locker(&m_mutex);
    return m_words;
}

int Reader::progress() const
{
    return m_progress;
}

QString Reader::source() const
{
    return m_source;
}

void Reader::setProgress(int progress)
{
    if (m_progress == progress)
        return;

    m_progress = progress;
    emit progressChanged(m_progress);
}

void Reader::setSource(QStringView source)
{
    if (m_source == source)
        return;

    m_source = source.toString();
    emit sourceChanged(m_source);
}

void Reader::addWord(const QString &word, int count)
{
    QMutexLocker locker(&m_mutex);

    const QString targetWord = m_caseSensetive ? word : word.toLower();

    m_words[targetWord] += count;

    updateTopList(targetWord);
}

void Reader::updateTopList(const QString &word)
{
    QMap<QString, int>::iterator topMapPos;
    for (topMapPos = m_top.begin(); topMapPos != m_top.end(); ++topMapPos)
    {
        if(m_words.value(word) > topMapPos.value())
        {
            m_top.insert(word, m_words.value(word));

            if(m_top.count() > m_topCount) //TODO: check if there items with some frequency (m_words.value(targetWord) == topMapPos.value() && (targetWord < topMapPos.key()))
            {
                m_top.erase(topMapPos);
            }

            break;
        }
    }

    if(topMapPos == m_top.end() && m_top.count() < m_topCount)
    {
        m_top.insert(word, m_words.value(word));
    }
}

int Reader::getTopCount() const
{
    return m_topCount;
}

void Reader::setTopCount(int topCount)
{
    m_topCount = topCount;
}

