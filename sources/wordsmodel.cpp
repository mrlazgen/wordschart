#include "wordsmodel.h"
#include "statistician.h"

#include <QDebug>
#include <random>
#include <algorithm>

WordsModel::WordsModel(QObject *parent)
    : QAbstractListModel(parent)
    , m_source(nullptr)
    , m_maxFrequency(0)
    , m_summary(0)
{
    m_colors = QColor::colorNames();

    std::random_device rd;
    std::mt19937 g(rd());
    std::shuffle(m_colors.begin(), m_colors.end(), g);
}

WordsModel::~WordsModel()
{

}

int WordsModel::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent)
    return m_items.count();
}

QVariant WordsModel::data(const QModelIndex &index, int role) const
{
    if (index.row() < 0 || index.row() > m_items.count())
        return QVariant();

    const auto word = m_items.keys().at(index.row());
    const auto count = m_items[word];

    switch (role) {
    case WordRole:
    case XRole:
        return word;
    case CountRole:
    case YRole:
        return count;
    case ColorRole:
        return m_colors.value(index.row(),"");
    default:
        return QVariant();
    }
}

QHash<int, QByteArray> WordsModel::roleNames() const
{
    const QHash<int, QByteArray> roles {
        { WordRole, "word" },
        { CountRole, "count" },
        { XRole, "x" },
        { YRole, "y" },
        { ColorRole, "color" }
    };
    return roles;
}

Statistician *WordsModel::source() const
{
    return m_source;
}

int WordsModel::count() const
{
    return m_items.count();
}

int WordsModel::maxFrequency() const
{
    return m_maxFrequency;
}

int WordsModel::summary() const
{
    return m_summary;
}

void WordsModel::setSource(Statistician *source)
{
    if (m_source == source)
        return;

    if(m_source)
        disconnect(m_source, &Statistician::topListChanged, this, &WordsModel::updateData);

    m_source = source;

    if(m_source)
        connect(m_source, &Statistician::topListChanged, this, &WordsModel::updateData);

    emit sourceChanged(m_source);

    updateData();
}

void WordsModel::setMaxFrequency(int maxFrequency)
{
    if (m_maxFrequency == maxFrequency)
        return;

    m_maxFrequency = maxFrequency;
    emit maxFrequencyChanged(m_maxFrequency);
}

void WordsModel::setSummary(int summary)
{
    if (m_summary == summary)
        return;

    m_summary = summary;
    emit summaryChanged(m_summary);
}

void WordsModel::updateData()
{
    if(!m_source)
    {
        clear();
        return;
    }

    emit layoutAboutToBeChanged();
    m_items = m_source->topList();
    emit layoutChanged();

    emit countChanged(count());

    const auto values = m_items.values();
    setSummary(std::accumulate(values.begin(), values.end(), 0));
    setMaxFrequency(*std::max_element(values.begin(), values.end()));
}

void WordsModel::clear()
{
    if(m_items.count() == 0)
        return;

    beginRemoveRows(QModelIndex(), 0, m_items.count() - 1);
    m_items.clear();
    endRemoveRows();

    emit countChanged(count());
}
