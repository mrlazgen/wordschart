#include "statistician.h"

#include <QTime>
#include <QElapsedTimer>

Statistician::Statistician(QObject *parent)
    : QObject(parent)
    , m_taskController(new TaskController())
    , m_progress(0)
    , m_working(false)
{
    m_taskController->moveToThread(&m_thread);

    connect(&m_thread, &QThread::started, m_taskController, &TaskController::run);
    connect(&m_reader, &Reader::progressChanged, this, &Statistician::setProgress);
    connect(&m_reader, &Reader::finished, this, &Statistician::finished);

    m_thread.start();
}

Statistician::~Statistician()
{

    m_thread.requestInterruption();
    m_thread.quit();
    m_thread.wait();

    delete m_taskController;
}

void Statistician::rate(const QString &file, int count)
{
    m_reader.setSource(file);
    m_reader.setTopCount(count);
    setWorking(true);

    m_taskController->push([&](){
        emit topListChanged();

        QElapsedTimer timer;
        timer.start();

        m_reader.read();

        setElapsedTime(QTime(0,0,0).addMSecs(timer.elapsed()).toString(QStringLiteral("m:s:z")));
        setWorking(false);
    });
}

int Statistician::progress() const
{
    return m_progress;
}

QMap<QString, int> Statistician::topList()
{
    return m_reader.getTop();
}

bool Statistician::working() const
{
    return m_working;
}

QString Statistician::elapsedTime() const
{
    return m_elapsedTime;
}

void Statistician::setProgress(int progress)
{
    if (m_progress == progress)
        return;

    m_progress = progress;
    emit progressChanged(m_progress);
    emit topListChanged();
}

void Statistician::setWorking(bool working)
{
    if (m_working == working)
        return;

    m_working = working;
    emit workingChanged(m_working);
    emit topListChanged();
}

void Statistician::setElapsedTime(const QString& elapsedTime)
{
    if (m_elapsedTime == elapsedTime)
        return;

    m_elapsedTime = elapsedTime;
    emit elapsedTimeChanged(m_elapsedTime);
}
