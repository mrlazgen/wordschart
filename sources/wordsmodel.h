#pragma once

#include <QAbstractListModel>
#include <QMap>
#include <QColor>

class Statistician;

class WordsModel : public QAbstractListModel
{
    Q_OBJECT
    Q_PROPERTY(Statistician* source READ source WRITE setSource NOTIFY sourceChanged)
    Q_PROPERTY(int count READ count NOTIFY countChanged)
    Q_PROPERTY(int maxFrequency READ maxFrequency WRITE setMaxFrequency NOTIFY maxFrequencyChanged)
    Q_PROPERTY(int summary READ summary WRITE setSummary NOTIFY summaryChanged)

    enum Roles {
        WordRole = Qt::UserRole + 1,
        CountRole,
        XRole,
        YRole,
        ColorRole
    };

public:
    explicit WordsModel(QObject *parent = nullptr);
    ~WordsModel();

    // QAbstractListModel interface
    int rowCount(const QModelIndex &parent) const override;
    QVariant data(const QModelIndex &index, int role) const override;
    QHash<int, QByteArray> roleNames() const override;

    Statistician* source() const;

    int count() const;

    int maxFrequency() const;

    int summary() const;

public slots:
    void setSource(Statistician* source);

    void setMaxFrequency(int maxFrequency);

    void setSummary(int summary);

signals:
    void sourceChanged(Statistician* source);

    void countChanged(int count);

    void maxFrequencyChanged(int maxFrequency);

    void summaryChanged(int summary);

private slots:
    void updateData();
    void clear();

private:
    QMap<QString, int> m_items;
    QStringList m_colors;
    Statistician* m_source;
    int m_maxFrequency;
    int m_summary;
};
