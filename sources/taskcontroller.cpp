#include "taskcontroller.h"

#include <QThread>
#include <QDebug>


TaskController::TaskController(QObject* parent)
    : QObject(parent)
{
}

TaskController::~TaskController()
{
}

void TaskController::run()
{
    qDebug() << "TaskController started at the thread:" << QThread::currentThreadId();

    forever {
        if ( QThread::currentThread()->isInterruptionRequested() ) {
            break;
        }

        if(m_tasksQueue.isEmpty()){
            QThread::msleep(10);
        }else{
            Element task = pop();
            task.function();
        }
    }

    qDebug() << "TaskController stopped at the thread:" << QThread::currentThreadId();
}

void TaskController::push(TaskFunction function)
{
    QMutexLocker locker(&m_mutex);
    Element task;
    task.function = function;
    m_tasksQueue.append(task);
}

TaskController::Element TaskController::pop()
{
    QMutexLocker locker(&m_mutex);
    return m_tasksQueue.takeFirst();
}

