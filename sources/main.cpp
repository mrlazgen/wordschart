#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>

#include "statistician.h"
#include "wordsmodel.h"

int main(int argc, char *argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    QCoreApplication::setOrganizationName("Yuriy Ibetullov");
    QCoreApplication::setApplicationName("Words Chart");

    QGuiApplication app(argc, argv);

    QQmlApplicationEngine engine;

    qmlRegisterType<Statistician>("Statistician", 1, 0, "Statistician");
    qmlRegisterType<WordsModel>("WordsModel", 1, 0, "TopWordsModel");

    const QUrl url(QStringLiteral("qrc:/main.qml"));
    QObject::connect(&engine, &QQmlApplicationEngine::objectCreated,
                     &app, [url](QObject *obj, const QUrl &objUrl) {
        if (!obj && url == objUrl)
            QCoreApplication::exit(-1);
    }, Qt::QueuedConnection);
    engine.load(url);

    return app.exec();
}
