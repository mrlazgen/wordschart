#pragma once

#include <QObject>
#include <QQueue>
#include <QMutex>
#include <QRunnable>

#include <functional>

typedef std::function <void()> TaskFunction;

class TaskController : public QObject, public QRunnable
{
    Q_OBJECT
    struct Element
    {
        TaskFunction function;
    };
public:
    explicit TaskController(QObject* parent = nullptr);
    ~TaskController();

public slots:
    void run() override;
    void push(TaskFunction function);

private:
    Element pop();

private:
    QMutex m_mutex;
    QQueue<Element> m_tasksQueue;
};
