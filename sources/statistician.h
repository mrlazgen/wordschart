#pragma once

#include <QObject>
#include <QThread>
#include <QVariant>
#include <QVariantMap>

#include "reader.h"
#include "taskcontroller.h"

class Statistician : public QObject
{
    Q_OBJECT

    Q_PROPERTY(int progress READ progress WRITE setProgress NOTIFY progressChanged)
    Q_PROPERTY(bool working READ working WRITE setWorking NOTIFY workingChanged)
    Q_PROPERTY(QString elapsedTime READ elapsedTime WRITE setElapsedTime NOTIFY elapsedTimeChanged)

public:
    explicit Statistician(QObject *parent = nullptr);
    ~Statistician();

    Q_INVOKABLE void rate(const QString &file, int count = 15);

    int progress() const;
    QMap<QString, int> topList();

    bool working() const;

    QString elapsedTime() const;

public slots:
    void setProgress(int progress);
    void setWorking(bool working);

    void setElapsedTime(const QString& elapsedTime);

signals:
    void progressChanged(int progress);
    void topListChanged();
    void finished(bool success, const QString& message);

    void workingChanged(bool working);

    void elapsedTimeChanged(const QString& elapsedTime);

private:
    TaskController *m_taskController;
    QThread m_thread;
    QMutex m_counterMutex;

    Reader m_reader;
    int m_progress;
    bool m_working;
    QString m_elapsedTime;
};
