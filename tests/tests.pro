QT += testlib
QT -= gui

CONFIG(debug, debug|release){
    DESTDIR = $$PWD/../bin/debug
} else {
    DESTDIR = $$PWD/../bin/release
}

CONFIG += qt console warn_on depend_includepath testcase c++17
CONFIG -= app_bundle

TEMPLATE = app

SOURCES += $$files(sources/*.cpp)

INCLUDEPATH += ../sources
HEADERS += \
        ../sources/reader.h
SOURCES += \
        ../sources/reader.cpp

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
else: target.path = $$DESTDIR

!isEmpty(target.path): INSTALLS += target

CONFIG += install_ok
