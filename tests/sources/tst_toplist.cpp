#include <QtTest>

#include "reader.h"

class TopList : public QObject
{
    Q_OBJECT

public:
    TopList();
    ~TopList();

private slots:
    void test_source();

    void test_topCount_data();
    void test_topCount();

    void test_read_data();
    void test_read();


};

TopList::TopList()
{

}

TopList::~TopList()
{

}

void TopList::test_source()
{
    Reader reader;
    reader.setSource(QStringLiteral("../../tests/data/empty.txt"));

    QCOMPARE(reader.source(), QStringLiteral("../../tests/data/empty.txt"));
}

void TopList::test_topCount_data()
{
    QTest::addColumn<int>("count");
    QTest::newRow("0") << 0;
    QTest::newRow("15") << 15;
    QTest::newRow("100") << 100;
    QTest::newRow("1000") << 1000;
    QTest::newRow("9999") << 9999;
    QTest::newRow("51603") << 51603;
}

void TopList::test_topCount()
{
    QFETCH(int, count);

    Reader reader;
    reader.setTopCount(count);
    QCOMPARE(reader.getTopCount(), count);
}


void TopList::test_read_data()
{
    QTest::addColumn<QString>("source");
    QTest::addColumn<int>("wordsCount");
    QTest::addColumn<int>("topCount");

    QTest::newRow("empty") << QStringLiteral("../../tests/data/empty.txt") << 0 << 0;
    QTest::newRow("4x4") << QStringLiteral("../../tests/data/4x4.txt") << 4 << 4;

    QTest::newRow("1") << QStringLiteral("../../tests/data/1.txt") << 1 << 1;
    QTest::newRow("5") << QStringLiteral("../../tests/data/5.txt") << 5 << 5;
    QTest::newRow("10") << QStringLiteral("../../tests/data/10.txt") << 10 << 10;
    QTest::newRow("20") << QStringLiteral("../../tests/data/20.txt") << 20 << 15;
    QTest::newRow("40") << QStringLiteral("../../tests/data/40.txt") << 40 << 15;
    QTest::newRow("50") << QStringLiteral("../../tests/data/50.txt") << 50 << 15;
    QTest::newRow("60") << QStringLiteral("../../tests/data/60.txt") << 60 << 15;
    QTest::newRow("70") << QStringLiteral("../../tests/data/70.txt") << 70 << 15;
    QTest::newRow("80") << QStringLiteral("../../tests/data/80.txt") << 80 << 15;
    QTest::newRow("90") << QStringLiteral("../../tests/data/90.txt") << 90 << 15;
    QTest::newRow("100") << QStringLiteral("../../tests/data/100.txt") << 100 << 15;
    QTest::newRow("200") << QStringLiteral("../../tests/data/200.txt") << 200 << 15;
    QTest::newRow("300") << QStringLiteral("../../tests/data/300.txt") << 300 << 15;
    QTest::newRow("400") << QStringLiteral("../../tests/data/400.txt") << 400 << 15;
    QTest::newRow("500") << QStringLiteral("../../tests/data/500.txt") << 500 << 15;
    QTest::newRow("600") << QStringLiteral("../../tests/data/600.txt") << 600 << 15;
    QTest::newRow("700") << QStringLiteral("../../tests/data/700.txt") << 700 << 15;
    QTest::newRow("800") << QStringLiteral("../../tests/data/800.txt") << 800 << 15;
    QTest::newRow("900") << QStringLiteral("../../tests/data/900.txt") << 900 << 15;
    QTest::newRow("1000") << QStringLiteral("../../tests/data/1000.txt") << 1000 << 15;
}

void TopList::test_read()
{
    QFETCH(QString, source);
    QFETCH(int, wordsCount);
    QFETCH(int, topCount);

    Reader reader;

    reader.setSource(source);
    QCOMPARE(reader.source(), source);

    QBENCHMARK {
        reader.read();
    }

    QCOMPARE(reader.getWords().count(), wordsCount);
    QCOMPARE(reader.getTop().count(), topCount);
}

QTEST_APPLESS_MAIN(TopList)

#include "tst_toplist.moc"
