import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Controls.Material 2.12

Item {
    id: root

    property Scale uiScale: Scale {}

    property alias model: topListView.model

    property string title: "title"
    property string yLabel: "yLabel"
    property string xLabel: "xLabel"

    property double yMaximum: 10
    property double yMinimum: 0
    property double yInterval: Math.pow(10, Math.floor(Math.log(yMaximum - yMinimum) / Math.LN10)) / ((Math.log(yMaximum - yMinimum) / Math.LN10) % 1 < 0.7 ? 4 : 2)

    implicitHeight: 500 * root.uiScale.yScale
    implicitWidth: 500 * root.uiScale.xScale

    Text {
        text: root.title
        anchors {
            horizontalCenter: plot.horizontalCenter
            bottom: plot.top
            bottomMargin: 30 * root.uiScale.yScale
        }
        font.pixelSize: 30 * root.uiScale.yScale
    }

    Text {
        text: root.yLabel
        font.pixelSize: 20 * root.uiScale.yScale
        anchors {
            verticalCenter: plot.verticalCenter
            right: plot.left
            rightMargin: 15 * root.uiScale.xScale
        }
        rotation: -90
    }

    Text {
        text: root.xLabel
        font.pixelSize: 20 * root.uiScale.yScale
        anchors {
            bottom: parent.bottom
            horizontalCenter: plot.horizontalCenter
        }
    }

    Item {
        id: plot

        function toYPixels(y){
            return plot.height / (root.yMaximum - root.yMinimum) * (y - root.yMinimum)
        }

        anchors {
            fill: parent
            topMargin: title.length > 0 ? 50 * root.uiScale.yScale : 20 * root.uiScale.yScale
            bottomMargin: 100 * root.uiScale.yScale
            leftMargin: 75 * root.uiScale.xScale
            rightMargin: 25 * root.uiScale.xScale
        }

        Repeater {
            model: Math.floor((yMaximum - yMinimum) / yInterval) + 1

            delegate: Rectangle {
                property double value: index * yInterval + yMinimum
                y: -plot.toYPixels(value) + plot.height
                width: plot.width
                height: 1
                color: "gray"

                Text {
                    text: parent.value
                    anchors {
                        right: parent.left
                        verticalCenter: parent.verticalCenter
                        margins: 5 * root.uiScale.xScale
                    }
                    horizontalAlignment: Text.AlignRight
                    font.pixelSize: 14 * root.uiScale.yScale
                }
            }
        }


        ListView {
            id: topListView
            anchors {
                fill: parent
            }
            orientation: ListView.Horizontal
            preferredHighlightBegin: 0.5 * width - (topListView.width / (topListView.count + topListView.spacing) / 2)
            preferredHighlightEnd: 0.5 * width - (topListView.width / (topListView.count + topListView.spacing)  / 2)
            highlightRangeMode: ListView.StrictlyEnforceRange
            currentIndex: topListView.count / 2
            interactive: false
            spacing: 5 * root.uiScale.xScale
            delegate: Item  {
                height: topListView.height
                width: topListView.width / topListView.count - topListView.spacing

                Rectangle {
                    width: parent.width
                    height: topListView.height * model.count / root.yMaximum
                    anchors {
                        bottom: parent.bottom
                    }
                    border {
                        width: 1
                        color: "gray"
                    }
                    color: model.color
                    Text {
                        anchors {
                            horizontalCenter: parent.horizontalCenter
                            top: parent.bottom
                            topMargin: width * 0.5
                        }
                        width: 70 * root.uiScale.yScale
                        horizontalAlignment: Text.AlignRight
                        font.pixelSize: 14 * root.uiScale.yScale
                        text: model.word
                        rotation: -90
                        elide: Text.ElideLeft
                    }

                    Text {
                        anchors {
                            horizontalCenter: parent.horizontalCenter
                            bottom: parent.top
                            bottomMargin: 10 * root.uiScale.yScale
                        }
                        horizontalAlignment: Text.AlignHCenter
                        font.pixelSize: 14 * root.uiScale.yScale
                        text: "%1".arg(model.count)
                    }

                    MouseArea {
                        id: mouseArea
                        anchors.fill: parent
                        hoverEnabled: true
                    }

                    Pane {
                        z: 99
                        Material.elevation: 3
                        visible: mouseArea.containsMouse
                        x: mouseArea.mouseX - width * 0.5
                        y: mouseArea.mouseY - height
                        Label {
                            text: model.word
                        }
                    }
                }
            }
            onCountChanged: topListView.currentIndex = topListView.count / 2
            onWidthChanged: topListView.currentIndex = topListView.count / 2
        }
    }
}
