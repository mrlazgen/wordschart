import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Controls.Material 2.12

Page {
    id: root
    property Scale uiScale: Scale {}

    property alias prevButtonText: prevButton.text
    property alias prevButtonVisible: prevButton.visible
    property alias nextButtonText: nextButton.text
    property alias nextButtonVisible: nextButton.visible

    signal next
    signal prev

    header: Pane {
        Material.elevation: 1
        Material.background: "#3C91E6"
        height: 50 * root.uiScale.yScale
        Label {
            anchors.centerIn: parent
            text: root.title
        }

        Button {
            id: prevButton
            highlighted: true
            visible: false
            anchors {
                verticalCenter: parent.verticalCenter
                left: parent.left
                leftMargin: 15 * root.uiScale.xScale
            }
            text: qsTr("Back")
            onClicked: root.prev()
        }

        Button {
            id: nextButton
            highlighted: true
            visible: false
            anchors {
                verticalCenter: parent.verticalCenter
                left: parent.left
                leftMargin: 15 * root.uiScale.xScale
            }
            text: qsTr("Back")
            onClicked: root.prev()
        }
    }

    background: Rectangle {
        color: "#D9D9D9"
    }
}
