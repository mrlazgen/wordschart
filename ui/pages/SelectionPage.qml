import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Controls.Material 2.12

import QtQuick.Dialogs 1.3

BasePage {
    id: root
    title: qsTr("File selection")

    property string path

    FileDialog {
        id: fileDialog
        title: qsTr("Please choose a file")
        folder: shortcuts.documents
        onAccepted: {
            root.path = fileDialog.fileUrl.toString().replace(/^(file:\/{3})/,"")
        }
        onRejected: {
        }
        nameFilters: [ "Text files (*.txt)" ]
    }

    Pane {
        id: fileInfoPane
        Material.elevation: 3
        anchors {
            left: parent.left
            right: parent.right
            top: parent.top
            bottom: parent.bottom //parent.verticalCenter

            margins: 10 * root.uiScale.xScale
        }

        Column {
            anchors {
                left: parent.left
                right: parent.right
                top: parent.top
                bottom: pickfileButton.top
                margins: 20 * root.uiScale.xScale
            }
            spacing: 25 * root.uiScale.yScale

            move: Transition {
                NumberAnimation { properties: "y"; duration: 150 }
            }

            Label {
                anchors.horizontalCenter: parent.horizontalCenter
                width: parent.width
                horizontalAlignment: Text.AlignHCenter

                text: root.path.length == 0 ? qsTr("Choose a file to count the frequency of occurrence of words in the file") : qsTr("Choosed file")
            }

            Label {
                visible: root.path.length > 0
                anchors.horizontalCenter: parent.horizontalCenter
                width: parent.width
                horizontalAlignment: Text.AlignHCenter
                text: qsTr("%1").arg(root.path)
            }
        }

        Button {
            id: pickfileButton
            anchors {
                horizontalCenter: parent.horizontalCenter
                bottom: parent.bottom
                bottomMargin: parent.height * 0.5 - height * 0.5
            }
            text: qsTr("Pick file")
            onClicked: {
                fileDialog.open()
            }
            Material.background: "#9FD356"
        }
    }

    Item {
        anchors {
            left: parent.left
            right: parent.right
            top: fileInfoPane.bottom
            bottom: parent.bottom
        }

        Button {
            id: rateButton
            Material.elevation: 6
            visible: false
            highlighted: true
            enabled: root.path.length > 0
            width: 200 * root.uiScale.xScale
            height: 150 * root.uiScale.yScale
            anchors {
                centerIn: parent
            }
            text: qsTr("Rate")
            onClicked: {
                root.next()
            }
        }
    }

    transitions: [
        Transition {
            NumberAnimation { target: pickfileButton; properties: "anchors.bottomMargin";  duration: 150 }
            NumberAnimation { target: pickfileButton; properties: "width";  duration: 150 }
            AnchorAnimation { duration: 150 }
        }
    ]

    states: [
        State {
            name: "fileSelected"
            when: root.path.length > 0
            PropertyChanges {
                target: pickfileButton;
                anchors.bottomMargin: 20 * root.uiScale.yScale
                text: qsTr("Pick another file")
            }
            AnchorChanges { target: fileInfoPane; anchors.bottom: parent.verticalCenter; }
            PropertyChanges {
                target: rateButton
                visible: true

            }
        }
    ]

}
