import QtQuick 2.12
import QtQml 2.12
import QtQuick.Controls 2.12
import QtQuick.Controls.Material 2.12

import Statistician 1.0
import WordsModel 1.0

import "../components"

BasePage {
    id: root
    title: qsTr("Top-%1 words").arg(topWordsModel.count)
    prevButtonVisible: true
    prevButtonText: processing ? qsTr("Cancel") : qsTr("Back")

    property int progress: statistician.progress
    property bool processing: timer.running || statistician.working
    property var elapsedTime: statistician.elapsedTime
    property string path

    TopWordsModel {
        id: topWordsModel
        source: Statistician {
            id: statistician

            onFinished: {
                if(!success) {
                    console.warn("Rate error:", message)
                    popup.error = message
                    popup.open()
                }
            }
        }
    }

    ProgressBar {
        id: progressBar
        anchors {
            left: parent.left
            right: parent.right
            top: parent.top
            margins: 10 * root.uiScale.xScale
        }

        value: statistician.progress
        from: 0
        to: 100

        background: Pane {
            Material.elevation: 3
            implicitHeight: 25 * root.uiScale.yScale
            implicitWidth: 200
        }

        contentItem: Item {
            implicitHeight: 25 * root.uiScale.yScale
            implicitWidth: 200
            Rectangle {
                width: progressBar.visualPosition * parent.width
                height: parent.height
                color: "#17a81a"
            }
        }

        Label {
            z: 99
            anchors.centerIn: parent
            text: root.processing ? qsTr("Progress: %1%").arg(root.progress) : qsTr("Elapsed time: %1".arg(root.elapsedTime))
        }
    }

    Pane {
        anchors {
            left: parent.left
            right: parent.right
            top: progressBar.bottom
            bottom: parent.bottom
            margins: 10 * root.uiScale.xScale
        }
        Material.elevation: 3

        HistogramView {
            id: topListView
            anchors.fill: parent
            visible: topWordsModel.count > 0
            uiScale: root.uiScale
            title:  ""
            yLabel: qsTr("Frequency")
            xLabel: qsTr("Word")
            yMaximum: topWordsModel.maxFrequency
            model: topWordsModel
        }

        Item {
            anchors.centerIn: parent
            visible: root.processing && topListView.model.count === 0
            BusyIndicator {
                id: buzyIndicator
                anchors.centerIn: parent
                running: visible
            }
            Label {
                anchors {
                    horizontalCenter: buzyIndicator.horizontalCenter
                    top: buzyIndicator.bottom
                    topMargin: 25 * root.uiScale.yScale
                }

                text: qsTr("Wait for data...")
            }
        }
    }

    Popup {
        id: popup
        property string error

        x: parent.width * 0.5 - width * 0.5
        y: parent.height * 0.5 - height * 0.5
        width: parent.width * 0.75
        height: parent.height * 0.75

        modal: true
        focus: true
        closePolicy: Popup.CloseOnEscape | Popup.CloseOnPressOutsideParent

        Column {
            anchors.centerIn: parent
            width: parent.width
            spacing: 25 * root.uiScale.yScale
            Label {
                anchors {
                    horizontalCenter: parent.horizontalCenter
                }
                width: parent.width
                horizontalAlignment: Text.AlignHCenter
                font.pixelSize: 24 * root.uiScale.xScale
                text: qsTr("Error")
            }

            Label {
                anchors {
                    horizontalCenter: parent.horizontalCenter
                }
                width: parent.width
                wrapMode: Text.WordWrap
                horizontalAlignment: Text.AlignHCenter
                text: popup.error
            }

            Button {
                anchors {
                    horizontalCenter: parent.horizontalCenter
                }
                text: qsTr("Back")
                onClicked: {
                    root.prev()
                }
            }
        }
        onClosed: {
            root.prev()
        }

    }

    Timer {
        id: timer //UI transition fix
        interval: 500
        repeat: false
        onTriggered: {
            statistician.rate(root.path)
        }
    }

    Component.onCompleted: {
        timer.start()
    }
}
