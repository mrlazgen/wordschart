import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.Controls 2.12

import "pages"

ApplicationWindow {
    id: window
    width: 800
    height: 600
    visible: true
    title: qsTr("Words frequency")

    Scale {
        id: appScale
        xScale: window.width / 800
        yScale: window.height / 600
    }

    QtObject {
        id: internal
        property string path
        property var pages: {
            "selection": selectionPageComponent,
            "result": resultPageComponent
        }

        function open(page) {
            stackView.push(pages[page])
        }
    }

    StackView {
        id: stackView
        anchors.fill: parent
        initialItem: selectionPageComponent
    }

    Component {
        id: selectionPageComponent
        SelectionPage {
            uiScale: appScale
            onPathChanged: {
                internal.path = path
            }

            onNext: {
                internal.open("result")
            }

            onPrev: {
                stackView.pop()
            }
        }
    }

    Component {
        id: resultPageComponent
        ResultPage {
            uiScale: appScale
            path: internal.path
            onPrev: {
                stackView.pop()
            }
        }
    }

}
